import React from 'react'
import { Header, Button, Icon } from 'semantic-ui-react'
import { Link } from 'react-router-dom'

function Home() {
  return (<div>
    <Header>Welcome to ProtectX!</Header>
    <p>Were we put the gun on user's hands to track down bad behaving sites!</p>
    <p>How does it work? It uses counterparty, and magic!</p>
    <p><Button as={Link} to='/about' color='blue'>Show me! <Icon name='chevron right'/></Button></p>
  </div>)
}

export default Home
