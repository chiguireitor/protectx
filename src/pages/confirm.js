import React, { useState, useCallback, useEffect } from 'react'
import { Header, Loader } from 'semantic-ui-react'
import axios from 'axios'

function Confirm() {
  const [confirmationStatus, setConfirmationStatus] = useState(0)
  const [currentCode, setCurrentCode] = useState(decodeURIComponent(window.location.search.slice(1)))
  const [targetSite, setTargetSite] = useState(null)

  const confirmCallback = useCallback(({ data }) => {
    setConfirmationStatus(data.status)
    setTargetSite(data.site)
  }, [setConfirmationStatus])

  useEffect(() => {
    axios.get(`/api/confirm/${currentCode}`).then(confirmCallback)
  }, [])

  return (<div>
    <Header>Confirm your comment</Header>
    <p>
    {confirmationStatus === 0 && <Loader active/>}
    {confirmationStatus !== 0 && <div>
      <p>{confirmationStatus}</p>
      {targetSite && <p>Go to the <a href={'/site?' + targetSite}>site</a></p>}
    </div>}
    </p>
  </div>)
}

export default Confirm
