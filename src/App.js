import React, { useState } from 'react'
import { Container, Menu, Input, Icon, Segment } from 'semantic-ui-react'
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom'

import 'semantic-ui-css/semantic.min.css'
import './App.css'

import Home from './pages/home'
import Site from './pages/site'
import About from './pages/about'
import Confirm from './pages/confirm'

function App(props) {
  const [siteSearch, setSiteSearch] = useState('')
  const [currentSite, setCurrentSite] = useState(decodeURIComponent(window.location.search.slice(1)))

  let routerObject
  const changeSearch = (e, { value }) => {
    setSiteSearch(value)
    setCurrentSite(value)

    routerObject.history.replace(`/site?${value}`)
  }

  const refRouter = (r) => {
    routerObject = r
  }

  return (
    <Router ref={refRouter}>
      <Container className="App">
        <Menu stackable inverted>
          <Menu.Item as={Link} to='/'>
            <img alt='ProtectX logo' src="/logo64.png"/>&nbsp;ProtectX
          </Menu.Item>

          <Menu.Item as={Link} to='/about'>
            About
          </Menu.Item>

          <Menu.Item position="right">
            <Input action={{ action: 'submit', content: 'Search', as: Link, to: `/site?${siteSearch}` }} value={siteSearch} placeholder="Website URL to search" onChange={changeSearch}/>
          </Menu.Item>
        </Menu>

        <Switch>
          <Route path="/" exact component={Home}/>

          <Route path="/about" component={About}/>
          <Route path="/confirm" render={() => (<Confirm />)}/>
          <Route path="/site" render={() => (<Site currentSite={currentSite}/>)}/>
        </Switch>

        <Segment inverted>
          <small><p>All rights reserved <Icon name='copyright outline'/> 2019 Ryan Baptiste. Made by John Villar with <Icon name='heart outline'/> in CCS/VE.</p></small>
        </Segment>
      </Container>
    </Router>
  )
}

export default App
