require('dotenv').config()

const express = require('express')
const jayson = require('jayson/promise')
const mariadb = require('mariadb')
const axios = require('axios')
const bs58check = require('bs58check')
const nodemailer = require('nodemailer')
const bodyParser = require('body-parser')
const emailValidator = require('email-validator')
const crypto = require('crypto')

const pool = mariadb.createPool({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: process.env.DB_NAME,
  connectionLimit: 5
})

const xchain = axios.create({ baseURL: 'https://xchain.io/api/' })
const xchainTestnet = axios.create({ baseURL: 'https://testnet.xchain.io/api/' })

const randomBytes = (num) => new Promise((resolve, reject) => {
  crypto.randomBytes(num, (err, data) => {
    if (err) {
      reject(err)
    } else {
      resolve(data)
    }
  })
})

const gravatarHash = (text) => {
  let md5 = crypto.createHash('md5')
  return md5.update(text.trim().toLowerCase()).digest('hex')
}

const EMAIL_TIMEOUT = 14400000
const CONFIRMED_TIMEOUT = 60000
const awaitingConfirmation = []

setInterval(() => {
  let idx = 0
  while (idx < awaitingConfirmation.length) {
    let conf = awaitingConfirmation[idx]
    if (conf.confirmed && conf.confirmed === 1) {
      conf.confirmed = 2
      conf.confirmedTs = Date.now()

      // TODO: Add to database

      idx++
    } else if (conf.confirmed && conf.confirmed === 2 && ((Date.now() - conf.confirmedTs) > CONFIRMED_TIMEOUT)) {
      awaitingConfirmation.splice(idx, 1)
    } else if ((Date.now() - conf.ts) > EMAIL_TIMEOUT) {
      awaitingConfirmation.splice(idx, 1)
    } else {
      idx++
    }
  }
}, 60000).unref()

const app = express()

app.use(bodyParser.json({ limit: process.env.JSON_BODY_LIMIT || '1mb' }))

app.get('/api/addr/:addr', async (req, res) => {
  try {
    let dec = bs58check.decode(req.params.addr)
  } catch (e) {
    res.status(400)
    res.json({error: 'bad-address'})
    return
  }

  try {
    console.log('Getting balances for', req.params.addr)
    let result = (await xchain.get(`/balances/${req.params.addr}`))
    res.json({bals: result.data})
  } catch (e) {
    console.log(e)
    res.status(500)
    res.json({error: 'internal'})
  }
})

app.get('/api/broadcasts/:addr', async (req, res) => {
  let versionByte = -1
  try {
    let dec = bs58check.decode(req.params.addr)
    versionByte = dec[0]
  } catch (e) {
    res.status(400)
    res.json({error: 'bad-address'})
    return
  }

  try {
    console.log('Getting broadcasts for', req.params.addr)

    let result
    if (versionByte === 0) {
      result = (await xchain.get(`/broadcasts/${req.params.addr}`))
    } else if (versionByte === 111) {
      result = (await xchainTestnet.get(`/broadcasts/${req.params.addr}`))
    }
    res.json({brds: result.data})
  } catch (e) {
    console.log(e)
    res.status(500)
    res.json({error: 'internal'})
  }
})

app.post('/api/comment/:site', async (req, res) => {
  if (!emailValidator.validate(req.body.email)) {
    res.json({ error: 'Bad email' })
    return
  }

  if (req.body.nickname.length > 64) {
    res.json({ error: 'Nickname too long' })
    return
  }

  if (req.body.comment.length > 1024) {
    res.json({ error: 'Comment too long' })
    return
  }

  let port = parseInt(process.env.EMAIL_PORT)
  let transporter = nodemailer.createTransport({
      host: process.env.EMAIL_SERVER,
      port: port,
      secure: port === 465,
      auth: {
          user: process.env.EMAIL_USER,
          pass: process.env.EMAIL_PASS
      }
  })

  let confirmCode = (await randomBytes(32)).toString('hex')
  let checkCode = (await randomBytes(8)).toString('hex')
  let urlConfirm = process.env.EMAIL_LINKBACK + `/confirm?${confirmCode}`

  let info = await transporter.sendMail({
    from: process.env.EMAIL_FROM,
    to: `"${req.body.nickname}" <${req.body.email}>`,
    subject: `Confirm your comment on ${process.env.EMAIL_SITENAME}`,
    text: `Go to ${urlConfirm} to confirm your comment:\n\n${req.body.comment}`,
    html: `Click <a href='${urlConfirm}'>this link</a> to confirm your comment:<br><br>${req.body.comment}`
  })

  if (info.accepted && info.accepted.indexOf(req.body.email) >= 0) {
    awaitingConfirmation.push({
      ts: Date.now(),
      code: confirmCode,
      check: checkCode,
      attachments: [],
      data: {site: req.params.site, ...req.body}
    })
    res.json({ ok: true, check: checkCode })
  } else {
    res.json({ error: 'Email address inexistent' })
  }
})

app.post('/api/attachment/:commentcheck', async (req, res) => {
  let comment = awaitingConfirmation.filter(x => x.check === req.params.commentcheck).pop()

  if (comment) {
    comment.attachments.push(req.body)
    res.json({ ok: true })
  } else {
    res.json({ error: 'Comment not found' })
  }
})

app.get('/api/attachment/:commentcode/:attachnum', async (req, res) => {
  let conn = await pool.getConnection()
  try {
    let attachment = await conn.query('SELECT * FROM attachment WHERE code=? ORDER BY ts LIMIT 1 OFFSET ?',
      [req.params.commentcode, parseInt(req.params.attachnum)])

    if (attachment.length === 0) {
      res.json({ error: 'Attachment not found' })
    } else {
      let att = attachment[0]
      res.json({ name: att.filename, type: att.mimetype, data: att.data.toString() })
    }
  } finally {
    conn.end()
  }
})

app.get('/api/comments/:site/:page', async (req, res) => {
  console.log(`Getting comments for ${req.params.site}`)
  let conn = await pool.getConnection()
  try {
    let offset = 0
    try {
      offset = parseInt(req.params.page)
      if (Number.isNaN(offset)) {
        offset = 0
      } else {
        offset *= 10
      }
    } catch (e) {}

    let comments = await conn.query('SELECT comment.code, comment, nickname, current_timestamp() - ts as deltatime, email, COALESCE(cnt, 0) cnt '+
        'FROM comment LEFT OUTER JOIN (SELECT code, site, count(*) cnt FROM attachment GROUP BY code, site) cnts ON cnts.code = comment.code AND cnts.site = comment.site ' +
        'WHERE comment.site=? ORDER BY ts DESC LIMIT 10 OFFSET ?', [req.params.site, offset])

    res.json(comments.map(x => {
      return {
        code: x.code,
        comment: x.comment,
        nickname: x.nickname,
        ts: x.deltatime * 1000,
        gravatar: gravatarHash(x.email),
        attachCnt: x.cnt
      }
    }))
  } catch (e) {
    console.log(e)
    res.json({ error: 'Error fetching comments' })
  } finally {
    conn.end()
  }
})

app.get('/api/check/:code', async (req, res) => {
  let conf = awaitingConfirmation.filter(x => x.check === req.params.code).pop()

  if (conf) {
    if (conf.confirmed) {
      res.json({ confirmed: true })
    } else {
      res.json({ waiting: true })
    }
  } else {
    res.json({ expired: true })
  }
})

app.get('/api/confirm/:code', async (req, res) => {
  console.log('Confirming code ', req.params)
  let conf = awaitingConfirmation.filter(x => x.code === req.params.code).pop()

  if (conf) {
    let conn = await pool.getConnection()
    try {
      await conn.query('INSERT INTO comment(code, site, nickname, email, comment) VALUES(?,?,?,?,?)',
        [conf.code, conf.data.site, conf.data.nickname, conf.data.email, conf.data.comment])

      for (let i=0; i < conf.attachments.length; i++) {
        let attach = conf.attachments[i]
        await conn.query('INSERT INTO attachment(code, site, filename, mimetype, data) VALUES(?,?,?,?,?)',
          [conf.code, conf.data.site, attach.name, attach.type, attach.data])
      }
      conf.confirmed = 1
      res.json({ status: 'Comment confirmed, you can close this tab now', site: conf.data.siteName })
    } catch (e) {
      console.log(e)
      res.json({ status: 'An error ocurred while confirming your comment, you can close this tab now' })
    } finally {
      conn.end()
    }

  } else {
    res.json({ status: 'Comment expired, you can close this tab now'})
  }
})

app.listen(parseInt(process.env.HTTP_PORT), async () => {
  console.log('Server listening on', process.env.HTTP_PORT)

  let conn = await pool.getConnection()
  try {
    let tables = await conn.query('show tables');
    let fieldName = `Tables_in_${process.env.DB_NAME}`
    const hasTable = (tname) => tables.filter(x => x[fieldName] === tname).length > 0

    if (!hasTable('comment')) {
      await conn.query('CREATE TABLE comment(code varchar(64), site varchar(256), ts TIMESTAMP default CURRENT_TIMESTAMP, nickname varchar(64), email varchar(256), comment TEXT, primary key(code, site));')
    }

    if (!hasTable('attachment')) {
      await conn.query('CREATE TABLE attachment(code varchar(64), site varchar(256), filename varchar(256), ts TIMESTAMP default CURRENT_TIMESTAMP, mimetype varchar(64), data MEDIUMBLOB, primary key(code, filename));')
    }
  } finally {
    conn.end()
  }
})
