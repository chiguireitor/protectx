import React, { useState, useEffect, useCallback } from 'react'
import { Header, Button, Grid, Popup, Loader, Icon, Segment, Feed, Form, Dimmer, Message, Image, Modal } from 'semantic-ui-react'
import { QRCode } from 'react-qr-svg'
import createHash from 'create-hash'
import bs58check from 'bs58check'
import axios from 'axios'
import ifIsImage from 'if-is-image'
import urlRegex from 'url-regex'
import Linkify from 'linkifyjs/react'
import {useDropzone} from 'react-dropzone'

function humanizeTime(t) {
  t = Math.round(t/1000)

  if (t < 60) {
    return t + ' seconds ago'
  } else if (t < 3600) {
    t = Math.round(t/60)
    return t + ' minutes ago'
  } else if (t < 86400) {
    t = Math.round(t / 3600)
    return t + ' hours ago'
  } else if (t < 604800) {
    t = Math.round(t / 86400)
    return t + ' days ago'
  } else if (t < 2419200) {
    t = Math.round(t / 604800)
    return t + ' weeks ago'
  } else if (t < 31536000) {
    t = Math.round(t / 2419200)
    return t + ' months ago'
  } else {
    t = Math.round(t / 31536000)
    return t + ' years ago'
  }
}

let tmoutUpdate
function Site(props) {
  const [loadingData, setLoadingData] = useState(true)
  const [siteName, setSiteName] = useState(props.currentSite)
  const [balanceData, setBalanceData] = useState(null)
  const [imageModalOpen, setImageModalOpen] = useState(false)
  const [imageModalImage, setImageModalImage] = useState(null)
  const [imageModalText, setImageModalText] = useState(null)
  const [imageModalTitle, setImageModalTitle] = useState(null)
  const calcAddress = (s) => {
    //return '1PDJv8u8zw4Fgqr4uCb2yim9fgTs5zfM4s' // This is for testing purposes only

    let h1 = createHash('sha256')
    let h2 = createHash('sha256')
    let r1 = createHash('ripemd160')
    h1.update(s.toLowerCase())
    let dgst = h1.digest()
    h2.update(dgst)
    r1.update(h2.digest())
    let rm = r1.digest()

    const payload = Buffer.allocUnsafe(21)
    payload.writeUInt8(0, 0)
    rm.copy(payload, 1)
    return bs58check.encode(payload)
  }
  const [siteAddress, setSiteAddress] = useState(() => calcAddress(siteName))
  const [xcpExtendedAddress, setXcpExtendedAddress] = useState(null)
  const [broadcasts, setBroadcasts] = useState(null)
  const [wallMessages, setWallMessages] = useState(null)
  const [wallPage, setWallPage] = useState(0)
  const [icon, setIcon] = useState(null)
  const [name, setName] = useState(siteName)
  const [nickname, setNickname] = useState(localStorage.getItem("defaultNickname") || '')
  const [email, setEmail] = useState(localStorage.getItem("defaultEmail") || '')
  const [comment, setComment] = useState('')
  const [commentPhase, setCommentPhase] = useState(0)
  const [commentError, setCommentError] = useState(null)
  const [commentStatus, setCommentStatus] = useState('')
  const [checkInterval, setCheckInterval] = useState(null)
  const [commentFiles, setCommentFiles] = useState([])

  const balanceCallback = useCallback(({ data }) => {
    setBalanceData(data.bals.data)
  }, [setBalanceData])

  const updateBalance = () => {
    if (!balanceData) {
      axios.get(`/api/addr/${siteAddress}`).then(balanceCallback)
    }
  }

  const findBroadcast = (brds, type, set) => {
    let remain = brds.filter(x => x.text.startsWith(type + ':'))

    if (remain.length > 0) {
      remain.forEach(x => x._used_ = true)
      set(remain[0].text.slice(type.length + 1))
    }
  }

  const broadcastsCallback = useCallback(({ data }) => {
    findBroadcast(data.brds.data, 'icon', setIcon)
    findBroadcast(data.brds.data, 'name', setName)

    setBroadcasts(data.brds.data.filter(x => !x._used_).map(b => {
      b.urls = b.text.match(urlRegex({strict: false}))
      b.images = []

      if (b.urls) {
        let i = 0
        while (i < b.urls.length) {
          if (ifIsImage(b.urls[i])) {
            b.images.push(b.urls.splice(i, 1).pop())
          } else {
            i++
          }
        }
      }

      return b
    }))
  }, [setBroadcasts, setIcon, setName])

  const updateBroadcasts = () => {
    if (!broadcasts) {
      axios.get(`/api/broadcasts/${siteName}`).then(broadcastsCallback)
    }
  }

  const updateComments = async () => {
    if (!wallMessages) {
      let result = await axios.get(`/api/comments/${siteAddress}/${wallPage}`) //.then(commentsCallback)
      setWallMessages(result.data)

      for (let i=0; i < result.data.length; i++) {
        let comment = result.data[i]

        if (comment.attachCnt > 0) {
          for (let j=0; j < comment.attachCnt; j++) {
            let attResult = await axios.get(`/api/attachment/${comment.code}/${j}`)

            if (!comment.attachments) {
              comment.attachments = []
            }

            comment.attachments.push(attResult.data)
          }
        }
        setWallMessages(result.data.map(x => x))
      }
    }
  }

  useEffect(() => {
    updateBalance()
    updateBroadcasts()
    updateComments()
  }, [siteAddress])
  //useEffect(updateComments, [siteAddress])

  useEffect(() => {
    setLoadingData(true)
    setBalanceData(null)
    setWallMessages(null)
    setSiteName(props.currentSite)
    setSiteAddress(calcAddress(props.currentSite))
    setName(props.currentSite)

    tmoutUpdate = setTimeout(async () => {
      tmoutUpdate = null
      try {
        let res = bs58check.decode(props.currentSite)

        if (res[0] === 0) {
          // Mainnet
          setXcpExtendedAddress('mainnet')
        } else if (res[0] === 111) {
          // Testnet
          setXcpExtendedAddress('testnet')
        }

      } catch (e) {
        setXcpExtendedAddress(null)
      }
      setLoadingData(false)
    }, 500)

    return () => {
      clearTimeout(tmoutUpdate)
    }
  }, [props.currentSite])

  const submitComment = async () => {
    localStorage.setItem("defaultNickname", nickname)
    localStorage.setItem("defaultEmail", email)

    setCommentPhase(1)
    setCommentStatus('Posting comment')
    let commentResult = await axios.post(`/api/comment/${siteAddress}`, {
      nickname, email, comment, siteName
    })

    if (commentResult.data.error) {
      setCommentPhase(0)
      setCommentError(commentResult.data.error)
    } else {
      let checkCode = commentResult.data.check

      if (commentFiles.length > 0) {
        setCommentStatus('Uploading attachments 1/' + commentFiles.length)
        for (let i=0; i < commentFiles.length; i++) {
          setCommentStatus('Uploading attachments ' + (i + 1) + '/' + commentFiles.length)
          let file = commentFiles[i]
          let base64String = file.binary //btoa(String.fromCharCode(...new Uint8Array(file.binary)))

          let result = await axios.post(`/api/attachment/${checkCode}`, {
            type: file.type, data: base64String, name: file.name
          })
        }
      }

      setCommentStatus('Check your email')
      let checkIntervalCode = setInterval(async () => {
        let result = await axios.get(`/api/check/${checkCode}`)

        if (result.data) {
          if (result.data.confirmed) {
            setCommentPhase(2)
            clearInterval(checkIntervalCode)
            setCheckInterval(null)
          } else if (result.data.expired) {
            setCommentPhase(3)
            clearInterval(checkIntervalCode)
            setCheckInterval(null)
          }
        }
      }, 5000)
      setCheckInterval(checkIntervalCode)
    }
  }

  const resetCommentForm = () => {
    setNickname(localStorage.getItem("defaultNickname") || '')
    setEmail(localStorage.getItem("defaultEmail") || '')
    setComment('')
    setCommentPhase(0)
    setCommentError(null)
    setCommentStatus(null)
    setCommentFiles([])

    if (checkInterval !== null) {
      clearInterval(checkInterval)
      setCheckInterval(null)
    }
  }

  const onDrop = useCallback(acceptedFiles => {
    let sem = acceptedFiles.length

    const fireSem = () => {
      sem--

      if (sem === 0) {
        let newFiles = commentFiles.concat(acceptedFiles)
        setCommentFiles(newFiles)
      }
    }

    acceptedFiles.forEach((file, idx) => {
      if (!file.type.startsWith('image/')) {
        return
      }

      const reader = new FileReader()
      reader.onabort = fireSem
      reader.onerror = fireSem
      reader.onload = (arg) => {
        let idx = reader.result.indexOf(';base64,')

        file.binary = reader.result.slice(idx + 8)

        fireSem()
      }

      reader.readAsDataURL(file)
    })
  }, [commentFiles])
  const {getRootProps, getInputProps, isDragActive} = useDropzone({onDrop})

  return (<Grid>
    <Grid.Row>
      <Grid.Column>
        <Header>Details for {siteName}</Header>
      </Grid.Column>
    </Grid.Row>

    <Grid.Row>
      <Grid.Column computer={6} mobile={16}>
        <Grid.Column width={16}>
          <QRCode
                bgColor="#FFFFFF"
                fgColor={loadingData?"#888888":"#000000"}
                level="Q"
                style={{ width: 256 }}
                value={loadingData?'loading data, please wait':siteAddress}
            />
          {!loadingData && <p><a href={`bitcoin:${siteAddress}`}>{siteAddress}</a>&nbsp;<Popup content={<div>
            <p>Fire your guns to this address to tag this site.</p>
            <p>This address is generated in a way that makes it impossible for it to exist a private key to move these assets.</p>
          </div>} trigger={<Button icon='question' circular color='blue' size='mini'/>}/></p>}
        </Grid.Column>

      </Grid.Column>

      <Grid.Column computer={10} mobile={16}>
        {xcpExtendedAddress && <Grid>
          <Grid.Column width={16}>
            <Segment>
              <Feed>
                {broadcasts && new Array(broadcasts.length).slice(0, 5).fill(0).map((_, idx) => {
                  let brd = broadcasts[idx]

                  return <Feed.Event key={idx}>
                    <Feed.Label>
                      {icon && <img alt="avatar" src={'//' + icon}  onClick={() => {
                        setImageModalImage('//' + icon)
                        setImageModalText('')
                        setImageModalTitle('Avatar for user ' + name)
                        setImageModalOpen(true)
                      }}/>}
                      {!icon && <Icon name='user'/>}
                    </Feed.Label>

                    <Feed.Content>
                      <Feed.Summary>
                        <Feed.User>{name}</Feed.User>
                        <Feed.Date>{(humanizeTime(Date.now() - brd.timestamp * 1000)) + ''}</Feed.Date>
                      </Feed.Summary>

                      <Feed.Extra>
                        <Linkify options={{ target: '_blank' }}>{brd.text}</Linkify>
                      </Feed.Extra>

                      {brd.images.length > 0 && <Feed.Extra images>
                        { new Array(brd.images.length).fill(0).map((_1, imgIdx) => {
                          return <Button key={idx + '_' + imgIdx} basic onClick={() => {
                            setImageModalImage('//' + brd.images[imgIdx])
                            setImageModalText(brd.text)
                            setImageModalTitle('Broadcast from ' + name)
                            setImageModalOpen(true)
                          }}>
                            <img alt="attachment" src={'//' + brd.images[imgIdx]} />
                          </Button>
                        }) }
                      </Feed.Extra>}
                    </Feed.Content>

                    <Feed.Meta>
                    </Feed.Meta>
                  </Feed.Event>
                })}
                {!broadcasts && <Loader active size='mini'>Loading broadcasts</Loader>}

              </Feed>
            </Segment>
          </Grid.Column>
        </Grid>}
        {loadingData && <Loader active size='massive'>Loading information</Loader>}
        {(!loadingData && balanceData && balanceData.length === 0) && <Segment basic><p>No guns have been fired to this URL</p><Icon size='massive' name='frown outline' color='grey'/></Segment>}
        {(!loadingData && balanceData && balanceData.length > 0) && (<Grid>
          <Grid.Column width={16}>
          <Header as='h2'>Fired guns to {siteName}</Header>
          </Grid.Column>
            {new Array(balanceData.length).fill(0).map((x, idx) => {
              let item = balanceData[idx]

              return <Grid.Column computer={4} mobile={8}>
                {item.asset_longname || item.asset} <small style={{ color: '#888888' }}>x{item.quantity}</small>
              </Grid.Column>
            })}
          </Grid>)}
      </Grid.Column>
    </Grid.Row>

    {/*xcpExtendedAddress*/ true && <Grid.Row>
      <Grid.Column width={16}>

        <Header as='h2'>Previous comments for {name}</Header>
        <Segment>
          <Popup content={<Form style={{ textAlign: 'left' }}>
            <Header as='h3'>Leave a comment for {name}</Header>
            <Form.Input label='Nickname' type='text' value={nickname} onChange={(e, { value }) => setNickname(value)}/>
            <Form.Input label='Email' type='text' value={email} onChange={(e, { value }) => setEmail(value)}/>
            <Form.TextArea label='Your comment' value={comment} onChange={(e, { value }) => setComment(value)}/>
            {commentError && <Message negative>{commentError}</Message>}
            <div className="dropArea" {...getRootProps()}>
              <div className="filesOverlay">
                {
                  new Array(commentFiles.length).fill(0).map((_, idx) => {
                    let file = commentFiles[idx]

                    if (file.type.startsWith('image/') && file.binary) {
                      let base64String = file.binary //btoa(String.fromCharCode(...new Uint8Array(file.binary)))

                      return <Image key={idx} src={'data:' + file.type +';base64,' + base64String} height={64} style={{ display: 'inline-block' }}/>
                    } else {
                      return <Icon key={idx} name='text file' size='big'/>
                    }
                  })
                }
              </div>
              <input {...getInputProps()} />
              {
                isDragActive ?
                  <div>
                    <p><Icon name="file text outline" size='big' className="jumpyFile"/></p>
                    <p>Drop the files here ...</p>
                  </div> :
                  <div>
                    <p><Icon name="file text outline" size='big'/></p>
                    <p>Drag files here, or click to select</p>
                  </div>
              }
            </div>
            <Form.Button onClick={submitComment}>Submit</Form.Button>
            {commentPhase !== 0 && <Dimmer active={commentPhase !== 0} inverted>
              {commentPhase === 1 && <Loader indeterminate size='massive' active>{commentStatus}</Loader>}
              {commentPhase === 2 && <div>
                <p><Icon size='massive' name='circle check outline' color='green'/></p>
                <p style={{ color: 'black' }}>Your comment will appear shortly</p></div>}
              {commentPhase === 3 && <div>
                <p><Icon size='massive' name='circle cancel outline' color='red'/></p>
                <p style={{ color: 'black' }}>Your comment expired</p></div>}
            </Dimmer>}
          </Form>} trigger={<Button icon='comment' circular color='blue' title='Add comment'/>}
          on='click' onOpen={resetCommentForm} onClose={resetCommentForm} position='top center' wide/>

          {!wallMessages && <div><p>No comments have been left for {name}</p></div>}
          {wallMessages && <Feed>
            { new Array(wallMessages.length).fill(0).map((_, idx) => {
              let comment = wallMessages[idx]

              return <Feed.Event key={idx}>
                <Feed.Label>
                  <img alt='avatar' src={'https://www.gravatar.com/avatar/' + comment.gravatar + '?d=mp'} onClick={() => {
                    setImageModalImage('https://www.gravatar.com/avatar/' + comment.gravatar + '?d=mp')
                    setImageModalText('')
                    setImageModalTitle('Gravatar for user ' + comment.nickname)
                    setImageModalOpen(true)
                  }}/>
                </Feed.Label>

                <Feed.Content>
                  <Feed.Summary>
                    <Feed.User>{comment.nickname}</Feed.User>
                    <Feed.Date>{humanizeTime(comment.ts)}</Feed.Date>
                  </Feed.Summary>

                  <Feed.Extra>
                    <Linkify options={{ target: '_blank' }}>{comment.comment}</Linkify>
                  </Feed.Extra>

                  {comment.attachCnt > 0 && comment.attachments && <Feed.Extra images>
                    { new Array(comment.attachments.length).fill(0).map((_1, imgIdx) => {
                      let att = comment.attachments[imgIdx]
                      let imageData = 'data:' + att.type + ';base64,' + att.data

                      return <Button key={idx + '_' + imgIdx} basic onClick={() => {
                        setImageModalImage(imageData)
                        setImageModalText(comment.comment)
                        setImageModalTitle('Comment attachment from ' + comment.nickname)
                        setImageModalOpen(true)
                      }}>
                        <Image src={imageData} />
                      </Button>
                    }) }
                  </Feed.Extra>}

                </Feed.Content>

                <Feed.Meta>
                </Feed.Meta>
              </Feed.Event>
            }) }
          </Feed>}
        </Segment>
      </Grid.Column>
    </Grid.Row>}

    <Modal
      open={imageModalOpen}
      onClose={() => setImageModalOpen(false)}
      basic size='small'>
      <Header icon='image' content={imageModalTitle}/>

      <Modal.Content>
        <Image src={imageModalImage}/>
        <p><Linkify options={{ target: '_blank' }}>{imageModalText}</Linkify></p>
      </Modal.Content>

      <Modal.Actions>
      </Modal.Actions>
    </Modal>
  </Grid>)
}

export default Site
