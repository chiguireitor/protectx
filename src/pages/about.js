import React from 'react'
import { Header } from 'semantic-ui-react'

function About() {
  return (<div>
    <Header>About ProtectX!</Header>
    <p>This is a site that let's you fire assets to a website!</p>
    <p>Using counterparty assets you can tag a website depending on the offense made by it. A public and immutable copy is left on the Bitcoin ledger forever and for everyone.</p>
  </div>)
}

export default About
